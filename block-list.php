<?php
$request_ip_blocklist = array (
    '151.106.0.0/16',
    '193.142.0.0/16',
    '84.17.0.0/16',
    '45.63.0.0/16',
    '128.199.0.0/16',
    '40.69.0.0/16',
    '46.20.0.0/16',
    '82.102.0.0/16',
    '138.3.0.0/16',
    '157.245.0.0/16',
    '185.152.0.0/16',
    '213.198.0.0/16',
    '46.246.0.0/16',
    '85.195.0.0/16'
);

$request_remote_addr = $_SERVER['REMOTE_ADDR'];
// Check if this IP is in blocklist.
if (!$request_ip_forbidden = in_array($request_remote_addr, $request_ip_blocklist)) {
    // Check if this IP is in CIDR block list.
    foreach ($request_ip_blocklist as $_cidr) {
        if (strpos($_cidr, '/') !== FALSE) {
            $_ip = ip2long($request_remote_addr);
            list ($_net, $_mask) = explode('/', $_cidr, 2);
            $_ip_net = ip2long($_net);
            $_ip_mask = ~((1 << (32 - $_mask)) - 1);

            if ($request_ip_forbidden = ($_ip & $_ip_mask) == ($_ip_net & $_ip_mask)) {
                break;
            }
        }
    }
}

if ($request_ip_forbidden) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}
?>